////
////  CoreDataHandler.swift
////  PersonFramework
////
////  Created by Emil Sandstrom on 2019-06-14.
////  Copyright © 2019 Emil. All rights reserved.
////
//
//import UIKit
//import CoreData
//
//extension Array {
//    var ElementType: Element.Type {
//        return Element.self
//    }
//}
//
//public class CoreDataHandler<T: Codable> {
//    
//    public init() {}
//    
//    public static func createData<T: Codable>(model: T) {
//        guard let entity = NSEntityDescription.entity(forEntityName: entityKey.rawValue, in: AppDelegate.shared.context) else {
//            return
//        }
//        let data = NSManagedObject(entity: entity, insertInto: AppDelegate.shared.context)
//        let mirror = Mirror(reflecting: model)
//        mirror.children.forEach { (child) in
//            data.setValue(child.value, forKey: child.label!)
//        }
//        save()
//        
//    }
//    
//    private func createMirro<T: Codable>(model: T) -> Mirror {
//        return Mirror(reflecting: model)
//    }
//    
//    /**
//     Delets object from CoreData
//     - Parameters:
//        - predicate: Is if you want specific items in the entity you look at
//        - model: is the Type of the model. i.e: write: Profile.self
//     - Precondition: 'model' needs to be the same as the type you cast to
//     i.e: let profile: [Profile]? = getModel(entityKey: .profile, model: Profile.self)
//
//     */
//    public static func get(predicate: NSPredicate? = nil) -> T? {
//        let fetchedResult = fetchResult(predicate: predicate)
//        let dictArray = getDataFromCoreData(results: fetchedResult)
//        if dictArray.count == 0 {
//            return nil
//        }
//        return encodeDecode(dictArray: dictArray)
//    }
//    
//    private static func encodeDecode(dictArray: [[String: Any]]) -> T? {
//        let isArray = String(describing: T.self).hasPrefix("Array<")
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: (isArray ? dictArray : dictArray.first ?? [[:]]),
//                                                      options: .prettyPrinted)
//            let theJSONText = String(data: jsonData,
//                                     encoding: String.Encoding.utf8)
//            let data = theJSONText?.data(using: String.Encoding.utf8)
//            let codableObject = try JSONDecoder().decode(T.self, from: data!)
//            return codableObject
//        } catch {
//            print(error.localizedDescription)
//            return nil
//        }
//    }
//    
//    /**
//     Delets object from CoreData
//     - Parameters:
//        - predicate: is what you will search for.
//        - model: is a need for deleting its childs
//     - Precondition: It is really not recommended to have a nil predicate, as it will delete every object for the entity.
//     
//     */
//    public static func delete(object predicate: NSPredicate? = nil) {
//        let results = fetchResult(predicate: predicate)
//        let dictArray = getDataFromCoreData(results: results)
//        let codable = encodeDecode(dictArray: dictArray)
//        results.forEach { (object) in
//            AppDelegate.shared.context.delete(object)
//        }
//        guard let id = getId(from: codable) else {
//            save()
//            return
//        }
//        deleteChild(parentId: id)
//        save()
//    }
//    
//    private static func getId(from model: T?) -> String? {
//        switch entityKey {
//        case .user:
//            guard let model = model as? User else {return nil}
//            return model.id
//        case .profile:
//            guard let model = model as? Profile else {return nil}
//            return model.id
//        case .spirometrySession:
//            guard let model = model as? SpirometrySession else {return nil}
//            return model.id
//        case .spirometryTest:
//            return nil
//        }
//    }
//    
//    private static func deleteChild(parentId: String) {
//        switch entityKey {
//        case .user:
//            CoreDataHandler<Profile>.delete(object: NSPredicate.generate(where: "userId", .equalTo, parentId))
//        case .profile:
//            CoreDataHandler<SpirometrySession>.delete(object: NSPredicate.generate(where: "profileId", .equalTo, parentId))
//        case .spirometrySession:
//            CoreDataHandler<SpirometryTest>.delete(object: NSPredicate.generate(where: "sessionId", .equalTo, parentId))
//        case .spirometryTest:
//            return
//            
//        }
//    }
//    
//    /**
//     Delets object from CoreData
//     - Parameters:
//        - predicate: is if you want to update a specific row for the entity
//        - newValue: is what constant will have it's value changed, where key is the constant and the element is the value
//     - Precondition: It is really not recommended to have a nil predicate, as it will update every object for the entity.
//     */
//    public static func update(object predicate: NSPredicate? = nil, newValue: [String: Any]) {
//        let results = fetchResult(predicate: predicate)
//        let key = newValue.keys.first!
//        print(key)
//        results.forEach { (result) in
//            result.setValue(newValue[key],
//                                forKey: key)
//        }
//        save()
//    }
//    
//    private static func save() {
//        do {
//            try AppDelegate.shared.context.save()
//        } catch let error {
//            print(error.localizedDescription)
//        }
//    }
//    
//    private static func fetchResult(predicate: NSPredicate? = nil) -> [NSManagedObject] {
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityKey.rawValue)
//        if let predicate = predicate {
//            request.predicate = predicate
//        }
//        guard let fetchedResult = try? AppDelegate.shared.context.fetch(request) as? [NSManagedObject] else {
//            return []
//        }
//        return fetchedResult
//    }
//    
//    private static func getDataFromCoreData(results: [NSManagedObject]) -> [[String: Any]] {
//        var dictArray: [[String: Any]] = Array(repeating: [:], count: results.count)
//        results.enumerated().forEach { (result) in
//            let mirror = Mirror(reflecting: objectType)
//            mirror.children.forEach { (child) in
//                let key = child.label!
//                dictArray[result.offset][key] = result.element.value(forKey: key)
//            }
//        }
//        return dictArray
//    }
//    
//    private static var entityKey: Entities {
//        switch T.self {
//        case is User.Type, is [User].Type:
//            return .user
//        case is Profile.Type, is [Profile].Type:
//            return .profile
//        case is SpirometrySession.Type, is [SpirometrySession].Type:
//            return .spirometrySession
//        case is SpirometryTest.Type, is [SpirometryTest].Type:
//            return .spirometryTest
//        default:
//            fatalError("This entity doesn't excist")
//        }
//    }
//    
//    private static var objectType: Codable {
//        switch T.self {
//        case is User.Type, is [User].Type:
//            return User()
//        case is Profile.Type, is [Profile].Type:
//            return Profile()
//        case is SpirometrySession.Type, is [SpirometrySession].Type:
//            return SpirometrySession()
//        case is SpirometryTest.Type, is [SpirometryTest].Type:
//            return SpirometryTest()
//        default:
//            fatalError("This entity doesn't excist")
//        }
//    }
//    
//}
