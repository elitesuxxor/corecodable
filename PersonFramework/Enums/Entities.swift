//
//  Entities.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-06-18.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation
import CoreData

public enum Entities: String {
    case profile = "Profile_entity"
    case spirometrySession = "SpirometrySession_entity"
    case spirometryTest = "SpirometryTest_entity"
    case user = "User_entity"
    
}
