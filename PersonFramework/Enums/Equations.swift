//
//  Equations.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-06-18.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation

public enum Equations: String {
    case equalTo = "=="
    case equalToOrGreaterThan = ">="
    case greaterThan = ">"
    case lesserThan = "<"
    case equalToOrLesserThan = "<="
}
