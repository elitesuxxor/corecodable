//
//  NSPredicate+extension.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-06-20.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation

public extension NSPredicate {
    
    static func generate(where searchKey: String, _ equation: Equations, _ value: String) -> NSPredicate {
        return NSPredicate(format: "\(searchKey)\(equation.rawValue)%@", value)
    }
    
    func validate(model: String) -> Bool {
        let searchName = self.predicateFormat.components(separatedBy: " ").first!
        return (CoreHandler.modelStorage[model]?.filter({$0.keys.contains(searchName)}).count ?? 0 > 0)
    }
}
