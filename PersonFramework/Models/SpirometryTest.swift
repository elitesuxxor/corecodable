////
////  SpirometryTest.swift
////  PersonFramework
////
////  Created by Emil Sandstrom on 2019-06-18.
////  Copyright © 2019 Emil. All rights reserved.
////
//
//import Foundation
//
//public struct SpirometryTest: Codable {
//    
//    public let id: String
//    public let fev1: Float
//    public let pef: Float
//    public let fvc: Float
//    public let flowArray: [Float]
//    public let sessionId: String
//    
//    public init(fev1: Float = 0, pef: Float = 0, fvc: Float = 0, flowArray: [Float] = [], id: String = "", sessionId: String = "") {
//        self.fev1 = fev1
//        self.pef = pef
//        self.fvc = fvc
//        self.flowArray = flowArray
//        self.id = id
//        self.sessionId = sessionId
//    }
//    
//    public var session: SpirometrySession? {
//        let predicate = NSPredicate.generate(where: "id", .equalTo, sessionId)
//        return CoreDataHandler<SpirometrySession>.get(predicate: predicate)
//    }
//}
