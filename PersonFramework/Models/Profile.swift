////
////  Profile.swift
////  PersonFramework
////
////  Created by Emil Sandstrom on 2019-06-18.
////  Copyright © 2019 Emil. All rights reserved.
////
//
//import Foundation
//
//public struct Profile: Codable {
//    public let id: String
//    public let height: Double
//    public let weight: Double
//    public let name: String
//    public let userId: String
//    
//    public init(height: Double = 1, name: String = "", weight: Double = 0, id: String = "", userId: String = "") {
//        self.height = height
//        self.weight = weight
//        self.name = name
//        self.id = id
//        self.userId = userId
//    }
//
//    public var user: User? {
//        let predicate = NSPredicate.generate(where: "id", .equalTo, userId)
//        return CoreDataHandler<User>.get(predicate: predicate)
//    }
//
//    public var sessions: [SpirometrySession]? {
//        let predicate = NSPredicate.generate(where: "profileId", .equalTo, id)
//        return CoreDataHandler<[SpirometrySession]>.get(predicate: predicate)
//    }
//}
