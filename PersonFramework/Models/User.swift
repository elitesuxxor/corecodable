////
////  User.swift
////  PersonFramework
////
////  Created by Emil Sandstrom on 2019-06-18.
////  Copyright © 2019 Emil. All rights reserved.
////
//
//import Foundation
//
//public struct User: Codable {
//    
//    public let email: String
//    public let id: String
//    
//    public init(email: String = "", id: String = "") {
//        self.email = email
//        self.id = id
//    }
//    
//    public var profiles: [Profile]? {
//        let predicate = NSPredicate.generate(where: "userId", .equalTo, id)
//        return CoreDataHandler<[Profile]>.get(predicate: predicate)
//    }
//}
