////
////  SpirometrySession.swift
////  PersonFramework
////
////  Created by Emil Sandstrom on 2019-06-18.
////  Copyright © 2019 Emil. All rights reserved.
////
//
//import Foundation
//
//public struct SpirometrySession: Codable {
//    
//    public let id: String
//    public let date: String
//    public let profileId: String
//    
//    public init(date: String = "", id: String = "", profileId: String = "") {
//        self.date = date
//        self.id = id
//        self.profileId = profileId
//    }
//    
//    public var getDate: Date {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-mm-dd"
//        return dateFormatter.date(from: self.date)!
//    }
//    
//    public var profile: Profile? {
//        let predicate = NSPredicate.generate(where: "id", .equalTo, profileId)
//        return CoreDataHandler<Profile>.get(predicate: predicate)
//    }
//    
//    public var tests: [SpirometryTest]? {
//        let predicate = NSPredicate.generate(where: "sessionId", .equalTo, id)
//        return CoreDataHandler<[SpirometryTest]>.get(predicate: predicate)
//    }
//}
