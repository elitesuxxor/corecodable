//
//  AppDelegate.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-06-18.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation
import CoreData

public class AppDelegate {
    
    static var shared = AppDelegate()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CodableTesting")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
}
