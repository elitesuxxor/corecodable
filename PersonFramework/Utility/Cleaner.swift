//
//  Cleaner.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-07-30.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation

extension String {
    
    var cleanName: String {
        var someString = self
        
        let rangeOpenCurl = someString.range(of: "(")
        let rangeCloseCurl = someString.range(of: someString)
        if let startLocation = rangeOpenCurl?.lowerBound,
            let endLocation = rangeCloseCurl?.upperBound {
            someString.replaceSubrange(startLocation ..< endLocation, with: "")
        }
        return someString
    }
    
    var cleanArray: String {
        var someString = self
        if someString.contains("Array<") {
            someString = someString.replacingOccurrences(of: "Array<", with: "")
            someString = someString.replacingOccurrences(of: ">", with: "")
            return "\(someString)_core"
        }
        return someString        
    }
    
    var getId: String {
        var someString = self
        return "\(String(someString.removeFirst()).lowercased())\(someString)Id"
    }
    
    var clean_Core: String {
        let name: String = self
        let endIndex = name.index(name.endIndex, offsetBy: -5)
        let truncated = name.substring(to: endIndex)
        return truncated
    }
}
