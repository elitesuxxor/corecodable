//
//  PersonFramework.h
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-06-14.
//  Copyright © 2019 Emil. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PersonFramework.
FOUNDATION_EXPORT double PersonFrameworkVersionNumber;

//! Project version string for PersonFramework.
FOUNDATION_EXPORT const unsigned char PersonFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PersonFramework/PublicHeader.h>


