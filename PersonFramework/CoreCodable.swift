//
//  CoreCodable.swift
//  PersonFramework
//
//  Created by Emil Sandstrom on 2019-07-30.
//  Copyright © 2019 Emil. All rights reserved.
//

import Foundation
import CoreData

public class CoreHandler {
    
    public static var modelStorage: [String: [[String: String]]] = ["test":[["test":"value"]]]
    
    public static func setup(models: [Codable]) {
        var internalModel: [String: [[String: String]]] = ["no_key_named_like_this_ever":[["yes":"no"]]]
        models.forEach { (model) in
            let mirr = Mirror.init(reflecting: model)
            mirr.children.forEach({ (child) in
                if internalModel["\(String(describing: model).cleanName)_core"] != nil {
                    internalModel["\(String(describing: model).cleanName)_core"]?.append([child.label!: String(describing: type(of: child.value)).cleanArray])
                } else {
                    internalModel["\(String(describing: model).cleanName)_core"] = [[child.label!: String(describing: type(of: child.value)).cleanArray]]
                }
            })
        }
        
        CoreHandler.modelStorage = internalModel
    }
}

public protocol CoreCodable: Codable {}

public extension CoreCodable {

    var context: NSManagedObjectContext {
        return AppDelegate.shared.persistentContainer.viewContext
    }
    
    static var context: NSManagedObjectContext {
        return AppDelegate.shared.persistentContainer.viewContext
    }
    
    var entityName: String {
        return "\(String(describing: self).cleanName)_core"
    }
    
    static var entityName: String {
        return "\(String(describing: self).cleanName)_core"
    }

    func createData() {
        creatingData(entityName: entityName)
    }
    
    private func creatingData(entityName: String) {
        var nestedValues: [String: [CoreCodable]] = [:]
        guard let entity = NSEntityDescription.entity(forEntityName: entityName,
                                                      in: context) else {
                                                        return
        }
        let data = NSManagedObject(entity: entity, insertInto: context)
        let mirror = Mirror(reflecting: self)
        mirror.children.forEach { (child) in
            for keysDict in CoreHandler.modelStorage[entityName]! {
                if keysDict.keys.contains(child.label!) {
                    let key = keysDict.keys.first!
                    if CoreHandler.modelStorage.keys.contains(keysDict[key]!) {
                        if let value = child.value as? [CoreCodable] {
                            nestedValues[keysDict[key] ?? ""] = value
                        }
                    }
                }
            }
            if child.value is [CoreCodable] {
                data.setValue([], forKey: child.label!)
            } else {
                data.setValue(child.value, forKey: child.label!)
            }
        }
        save()
        
        if !nestedValues.isEmpty {
            nestedValues.forEach { (key, coreCodable) in
                coreCodable.forEach({ (codable) in
                    codable.creatingData(entityName: key)
                })
            }
        }
    }
    
    static func fetchOne(predicate: NSPredicate? = nil) -> Self? {
        return handleFetch(predicate: predicate)?.first
    }
    
    static func fetchMany(predicate: NSPredicate? = nil) -> [Self]? {
        return handleFetch(predicate: predicate)
    }
    
    private static func handleFetch(predicate: NSPredicate? = nil) -> [Self]? {
        let fetchedResult = fetchResult(predicate: predicate)
        let dictArray = getDataFromCoreData(results: fetchedResult)
        if dictArray.count == 0 {
            return nil
        }
        guard let encoded: [Self] = encodeDecode(dictArray: dictArray) else {
            return nil
        }
        
        return encoded
    }
    
    static func update(object predicate: NSPredicate? = nil, newValue: [String: Any]) {
        let results = fetchResult(predicate: predicate)
        let key = newValue.keys.first!
        results.forEach { (result) in
            result.setValue(newValue[key],
                            forKey: key)
        }
        save()
    }
    
    static func delete(object predicate: NSPredicate? = nil) {
        let results = fetchResult(predicate: predicate)
        let dictArray = getDataFromCoreData(results: results)
        
        guard let encoded: [Self] = encodeDecode(dictArray: dictArray) else {return}
        encoded.forEach { (core) in
            let mirror = Mirror(reflecting: core)
            var id = ""
            var values: [CoreCodable] = []
            mirror.children.forEach({ (child) in
                if let value = child.value as? [CoreCodable] {
                    values = value
                    if id != "" {
                        value.forEach({ (nestedStruct) in
                            nestedStruct.internalDelete(with: id, internalId: String(describing: self).cleanName.getId)
                        })
                    }
                } else if child.label! == "id" {
                    id = "\(child.value)"
                    if !values.isEmpty {
                        values.forEach({ (nestedStruct) in
                            nestedStruct.internalDelete(with: id, internalId: String(describing: self).cleanName.getId)
                        })
                    }
                }
            })
        }
        results.forEach { (object) in
            context.delete(object)
        }
        save()
    }
    
    func delete() {
        var id = ""
        let mirror = Mirror(reflecting: self)
        mirror.children.forEach { (child) in
            if child.label! == "id" {
                id = "\(child.value)"
            }
        }
        
        internalDelete(with: id, internalId: "id")
    }
    
    private func internalDelete(with id: String, internalId: String) {
        let results = fetchResult(predicate: NSPredicate.generate(where: internalId, .equalTo, id))
        let dictArray = getDataFromCoreData(results: results)
        guard let encoded: [Self] = encodeDecode(dictArray: dictArray) else {return}
        encoded.forEach { (core) in
            let mirror = Mirror(reflecting: core)
            var id = ""
            var values: [CoreCodable] = []
            mirror.children.forEach({ (child) in
                if let value = child.value as? [CoreCodable] {
                    values = value
                    if id != "" {
                        value.forEach({ (nestedStruct) in
                            nestedStruct.internalDelete(with: id, internalId: String(describing: self).cleanName.getId)
                        })
                    }
                } else if child.label! == "id" {
                    id = "\(child.value)"
                    if !values.isEmpty {
                        values.forEach({ (nestedStruct) in
                            nestedStruct.internalDelete(with: id, internalId: String(describing: self).cleanName.getId)
                        })
                    }
                }
            })
        }
        results.forEach { (object) in
            context.delete(object)
        }
        save()
    }
    
    private static func fetchResult(predicate: NSPredicate? = nil, tempEntityName: String? = nil) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: tempEntityName == nil ? entityName : tempEntityName!)
        if let predicate = predicate {
            request.predicate = predicate
            
            if !predicate.validate(model: tempEntityName == nil ? entityName : tempEntityName!) {
                return []
            }
        }
        
        guard let fetchedResult = try? context.fetch(request) as? [NSManagedObject] else {
            return []
        }
        return fetchedResult
    }
    
    private static func getDataFromCoreData(results: [NSManagedObject], tempEntityName: String? = nil) -> [[String: Any]] {
        var dictArray: [[String: Any]] = Array(repeating: [:], count: results.count)
        results.enumerated().forEach { (result) in
            if let keys = CoreHandler.modelStorage[tempEntityName == nil ? entityName : tempEntityName!] {
                keys.forEach({ keyArray in
                    let key = keyArray.keys.first!
                    if CoreHandler.modelStorage.keys.contains(keyArray[key]!) && key != (tempEntityName == nil ? entityName : tempEntityName!) {
                        let innerResults = fetchResult(predicate: NSPredicate.generate(where: String(describing: self).cleanName.getId,
                                                                                       .equalTo,
                                                                                       dictArray[result.offset]["id"] as! String),
                                                       tempEntityName: keyArray[key])
                        let innerDictArray = getDataFromCoreData(results: innerResults, tempEntityName: keyArray[key])
                        dictArray[result.offset][key] = innerDictArray
                    } else {
                        dictArray[result.offset][key] = result.element.value(forKey: key)
                    }
                })
            }
        }
        return dictArray
    }
    
    private static func encodeDecode<T: CoreCodable>(dictArray: [[String: Any]]) -> [T]? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictArray,
                                                      options: .prettyPrinted)
            let theJSONText = String(data: jsonData,
                                     encoding: String.Encoding.utf8)
            let data = theJSONText?.data(using: String.Encoding.utf8)
            let codableObject = try JSONDecoder().decode([T].self, from: data!)
            return codableObject
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    private func save() {
        do {
            try context.save()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    private static func save() {
        do {
            try context.save()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    private func fetchResult(predicate: NSPredicate? = nil, tempEntityName: String? = nil) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: tempEntityName == nil ? entityName : tempEntityName!)
        if let predicate = predicate {
            request.predicate = predicate
            
            if !predicate.validate(model: tempEntityName == nil ? entityName : tempEntityName!) {
                return []
            }
        }
        
        guard let fetchedResult = try? context.fetch(request) as? [NSManagedObject] else {
            return []
        }
        return fetchedResult
    }
    
    private func getDataFromCoreData(results: [NSManagedObject], tempEntityName: String? = nil) -> [[String: Any]] {
        var dictArray: [[String: Any]] = Array(repeating: [:], count: results.count)
        results.enumerated().forEach { (result) in
            if let keys = CoreHandler.modelStorage[tempEntityName == nil ? entityName : tempEntityName!] {
                keys.forEach({ keyArray in
                    let key = keyArray.keys.first!
                    if CoreHandler.modelStorage.keys.contains(keyArray[key]!) && key != (tempEntityName == nil ? entityName : tempEntityName!) {
                        let innerResults = fetchResult(predicate: NSPredicate.generate(where: String(describing: self).cleanName.getId,
                                                                                       .equalTo,
                                                                                       dictArray[result.offset]["id"] as! String),
                                                       tempEntityName: keyArray[key])
                        let innerDictArray = getDataFromCoreData(results: innerResults, tempEntityName: keyArray[key])
                        dictArray[result.offset][key] = innerDictArray
                    }
                    dictArray[result.offset][key] = result.element.value(forKey: key)
                })
            }
        }
        return dictArray
    }
    
    private func encodeDecode<T: CoreCodable>(dictArray: [[String: Any]]) -> [T]? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictArray,
                                                      options: .prettyPrinted)
            let theJSONText = String(data: jsonData,
                                     encoding: String.Encoding.utf8)
            let data = theJSONText?.data(using: String.Encoding.utf8)
            let codableObject = try JSONDecoder().decode([T].self, from: data!)
            return codableObject
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
